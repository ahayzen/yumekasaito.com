#!/bin/bash

# Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

NAME="yumekasaito.com"                                  # Name of the application
DJANGODIR="/srv/webapp/yumekasaito.com/"             # Django project directory
# SOCKFILE="/srv/webapp/yumekasaito.com/run/gunicorn.sock"  # we will communicate using this unix socket
ADDR="0.0.0.0:8080"
# USER=djangouser                                        # the user to run as
# GROUP=webapps                                     # the group to run as
NUM_WORKERS=3                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE="yumekasaito.settings.production"             # which settings file should Django use
DJANGO_WSGI_MODULE="yumekasaito.wsgi"                     # WSGI module name

# Activate the virtual environment
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
# RUNDIR=$(dirname $SOCKFILE)
# test -d "$RUNDIR" || mkdir -p "$RUNDIR"

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
#
# Redirect stderr to stdout so that we can look for debug lines in stdout
exec gunicorn -b "$ADDR" "${DJANGO_WSGI_MODULE}":application \
  --name "$NAME" \
  --workers "$NUM_WORKERS" \
  --log-level=debug \
  --log-file=- \
  2>&1
  # --user=$USER --group=$GROUP \
