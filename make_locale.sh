#!/bin/sh

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

python3 "$SCRIPTPATH/manage.py" makemessages -l ja
python3 "$SCRIPTPATH/manage.py" compilemessages
