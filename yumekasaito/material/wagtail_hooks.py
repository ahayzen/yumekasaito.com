#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2017, 2018
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from wagtail.core import hooks


# FIXME: HACK: This is a hack to ensure that when editing images in the wagtail admin
# the materials_en and materials_en fields use taggit for selecting tags
# wagtail itself has a special case for the field "tags"

# It runs when the page is first loaded, eg when editing an image in /admin/images
# It runs when a .modal is focused, eg when a using the image chooser

# References in wagtail code
# Add: https://github.com/wagtail/wagtail/blob/c6666c6de5e83bf94d18324858c121e6584ba47d/wagtail/wagtailimages/
# templates/wagtailimages/images/add.html#L11
# Edit: https://github.com/wagtail/wagtail/blob/c6666c6de5e83bf94d18324858c121e6584ba47d/wagtail/wagtailimages/
# templates/wagtailimages/images/edit.html#L17
# Chooser: https://github.com/wagtail/wagtail/blob/c6666c6de5e83bf94d18324858c121e6584ba47d/wagtail/wagtailimages/
# templates/wagtailimages/chooser/chooser.js#L106

@hooks.register("insert_global_admin_js")
def global_admin_js():
    # TODO: /admin-material/material-en-autocomplete/ should be {% url 'material_en_autocomplete' as autocomplete_url %}
    return """
<script type="text/javascript">
function addMaterialTagit() {
    $('#id_materials_ja').tagit({
        autocomplete: {source: "/admin-material/material-ja-autocomplete/"}
    });
    $('#id_materials_en').tagit({
        autocomplete: {source: "/admin-material/material-en-autocomplete/"}
    });
}

$(function() {
    addMaterialTagit();
});

$(document).on("focus", ".modal", function() {
    addMaterialTagit();
});
</script>
        """
