#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2017, 2018
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.http import JsonResponse

from yumekasaito.material.models import MaterialEnTag, MaterialJaTag


def material_base(request, cls):
    term = request.GET.get('term', None)
    if term:
        tags = cls.objects.filter(name__istartswith=term).order_by('name')
    else:
        tags = cls.objects.none()

    return JsonResponse([tag.name for tag in tags], safe=False)


def material_en(request):
    return material_base(request, MaterialEnTag)


def material_ja(request):
    return material_base(request, MaterialJaTag)
