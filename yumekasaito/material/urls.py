#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2017, 2018
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.conf.urls import url

from .views import autocomplete

urlpatterns = [
    url(r'^material-en-autocomplete/$', autocomplete.material_en, name='material_en_autocomplete'),
    url(r'^material-ja-autocomplete/$', autocomplete.material_ja, name='material_ja_autocomplete'),
]
