#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2017, 2018
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.db import models

from taggit.models import TagBase, GenericTaggedItemBase


# Add models for storing the tags per language
class MaterialEnTag(TagBase):
    class Meta:
        verbose_name = "Material"
        verbose_name_plural = "Materials"


class MaterialJaTag(TagBase):
    class Meta:
        verbose_name = "Material"
        verbose_name_plural = "Materials"


# Add models for putting as the 'through' in TaggableManager
class MaterialEn(GenericTaggedItemBase):
    tag = models.ForeignKey(MaterialEnTag,
                            related_name="%(app_label)s_%(class)s_items_en",
                            on_delete=models.CASCADE)


class MaterialJa(GenericTaggedItemBase):
    tag = models.ForeignKey(MaterialJaTag,
                            related_name="%(app_label)s_%(class)s_items_ja",
                            on_delete=models.CASCADE)
