#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2016, 2018, 2020
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from __future__ import absolute_import, unicode_literals
from django.db import models

from yumekasaito.base.models import YumekaPage, YumekaBodyMixin


class HomePage(YumekaPage, YumekaBodyMixin):
    subpage_types = [
        "blog.BlogIndexPage",
        "section.SectionIndexPage",
        "form.FormPage",
        "event.EventIndexPage",
    ]
    parent_page_types = []

    # Override YumekaTitleTranslationMixin so we default to False
    automatic_title_heading = models.BooleanField(
        default=False,
        help_text="Should a header in the body be created from the title?")

    content_panels = YumekaPage.content_panels + YumekaBodyMixin.content_panels

    search_fields = YumekaPage.search_fields + YumekaBodyMixin.search_fields

    class Meta:
        verbose_name = "Home Page"
