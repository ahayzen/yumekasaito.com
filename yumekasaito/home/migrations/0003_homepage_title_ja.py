# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-21 00:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_create_homepage'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='title_ja',
            field=models.CharField(default='Hello!', max_length=255),
            preserve_default=False,
        ),
    ]
