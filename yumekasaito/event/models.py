#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2021
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.db import models
from django.shortcuts import render
from django.utils import translation
from django.utils.functional import cached_property

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core.blocks import (
    ListBlock, PageChooserBlock, StreamBlock, StructBlock
)
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel

from yumekasaito.base.models import (
    YumekaPage, YumekaBodyMixin, YumekaListingImageMixin,
    YumekaPaginationMixin,
)


class EventIndexPage(YumekaPage, YumekaListingImageMixin,
                     YumekaPaginationMixin):
    parent_page_types = [
        "home.HomePage",
    ]
    subpage_types = [
        "event.ConceptPage",
    ]

    content_panels = (
        YumekaPage.content_panels
    )

    promote_panels = (
        YumekaPage.promote_panels + YumekaListingImageMixin.promote_panels
    )

    search_fields = (
        YumekaPage.search_fields
    )

    @cached_property
    def children(self):
        return self.get_children().live().specific().order_by("-first_published_at")

    def serve(self, request, *args, **kwargs):
        paginated_children = self.get_paginator_results(request, self.children)

        return render(
            request,
            self.template,
            {
                'self': self,
                'children': paginated_children,
            }
        )

    class Meta:
        verbose_name = "Event Index"


# Concept page
#
# this has text at the top
# gallery below which uses image from child page
class ConceptPage(YumekaPage, YumekaListingImageMixin, YumekaBodyMixin):
    parent_page_types = [
        "event.EventIndexPage",
    ]
    subpage_types = [
        "event.ConceptWorkPage",
    ]

    date = models.DateField(blank=False, verbose_name="Date of the concept")

    content_panels = (
        YumekaPage.content_panels +
        [
            FieldPanel("date")
        ] +
        YumekaBodyMixin.content_panels
    )

    promote_panels = (
        YumekaPage.promote_panels + YumekaListingImageMixin.promote_panels
    )

    search_fields = (
        YumekaPage.search_fields +
        YumekaBodyMixin.search_fields
    )

    @cached_property
    def children(self):
        return self.get_children().live().specific()

    class Meta:
        verbose_name = "New Concept"


class ConceptWorkLinkBlock(StructBlock):
    link = PageChooserBlock(label="Page", required=True)

    class Meta:
        icon = "link"


# Is the one QR code goes to
#
# has image on left, text on right
# links to about and concept using square, grey with text inside buttons
class ConceptWorkPage(YumekaPage, YumekaListingImageMixin, YumekaBodyMixin):
    parent_page_types = [
        "event.ConceptPage",
    ]
    subpage_types = [
    ]

    # We need to set both blank and required
    # https://github.com/wagtail/wagtail/issues/4306
    links = StreamField([('links', ConceptWorkLinkBlock(required=False))], blank=True)

    @cached_property
    def default_links(self):
        return list(Page.objects.filter(slug='about')[:1])

    content_panels = (
        YumekaPage.content_panels +
        # We want the listing image to be in the main content panels
        YumekaListingImageMixin.promote_panels +
        YumekaBodyMixin.content_panels +
        [
            StreamFieldPanel("links"),
        ]
    )

    promote_panels = (
        YumekaPage.promote_panels
    )

    search_fields = (
        YumekaPage.search_fields +
        YumekaBodyMixin.search_fields
    )

    def get_next_live_sibling(self):
        return self.get_next_siblings(
            inclusive=False,
        ).filter(live=True).first()

    def get_previous_live_sibling(self):
        return self.get_prev_siblings(
            inclusive=False,
        ).filter(live=True).first()

    class Meta:
        verbose_name = "New Work"
