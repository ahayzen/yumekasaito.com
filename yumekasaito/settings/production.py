from __future__ import absolute_import, unicode_literals

from .base import *

ALLOWED_HOSTS = ["*"]
DEBUG = False

# minify CSS
COMPRESS_CSS_FILTERS = [
    'compressor.filters.cssmin.CSSMinFilter',
]

RECAPTCHA_PUBLIC_KEY = "MyRecaptchaKey123"
RECAPTCHA_PRIVATE_KEY = "MyRecaptchaPrivateKey456"

# Production paths
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': "/srv/webapp/db/db.sqlite3",
    }
}

STATIC_ROOT = "/srv/webapp/static/"
MEDIA_ROOT = "/srv/webapp/media/"

BASE_URL = "http://www.yumekasaito.com/"

try:
    from .local import *
except ImportError:
    pass
