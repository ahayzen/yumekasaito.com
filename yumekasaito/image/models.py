#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2016, 2017, 2018, 2020
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.utils.functional import cached_property
from subprocess import run as subprocess_run
from tempfile import NamedTemporaryFile
from taggit.managers import TaggableManager

from wagtail.core.fields import RichTextField
from wagtail.images.fields import WagtailImageField
from wagtail.images.models import AbstractImage, AbstractRendition

from yumekasaito.base.translation import TranslatedField
from yumekasaito.material.models import MaterialEn, MaterialJa


def to_python(cls, data):
    """Here we reimplement the WagtailImageField.to_python method"""

    # Remove any alpha channels from PNGs as Pillow fails
    # https://stackoverflow.com/questions/41576637/are-rgba-pngs-unsupported-in-python-3-5-pillow
    #
    # WagtailImageFile.to_python
    #   - ImageField.to_python  -> Pillow.open
    #
    # Here we manually convert the files to have no alpha
    #
    # This is a rewrite of
    # https://github.com/wagtail/wagtail/blob/master/wagtail/images/fields.py#L108
    if data.name.endswith(".png"):
        # Write the memory file into temporary file
        with NamedTemporaryFile() as tmp_f:
            tmp_f.write(data.read())

            # Remove the alpha from the file
            subprocess_run([
                "/usr/bin/convert",
                tmp_f.name,
                "-background",
                "white",
                "-flatten",
                tmp_f.name
            ])

            tmp_f.seek(0)
            data.seek(0)

            # Read back the new file into the memory file
            data.truncate()
            data.write(tmp_f.read())

            data.seek(0)

    f = super(WagtailImageField, cls).to_python(data)

    if f is not None:
        cls.check_image_file_size(f)
        cls.check_image_file_format(f)
        cls.check_image_pixel_size(f)

    return f


WagtailImageField.to_python = to_python


class YumekaImage(AbstractImage):
    title_ja = models.CharField(
        max_length=255, blank=True, verbose_name="Title Japanese",
    )
    translated_title = TranslatedField("title")

    concept_en = RichTextField(
        blank=True, verbose_name="Concept English",
        features=["h2", "h3", "h4", "bold", "italic"],
    )
    concept_ja = RichTextField(
        blank=True, verbose_name="Concept Japanese",
        features=["h2", "h3", "h4", "bold", "italic"],
    )
    concept = TranslatedField("concept")

    # FIXME: This has a hack in material.wagtail_hooks to make them use taggit in the wagtail editor
    # ensure that the field names keep insync with the javascript id used
    materials_en = TaggableManager(through=MaterialEn, help_text=None, blank=True, verbose_name='Materials En')
    materials_ja = TaggableManager(through=MaterialJa, help_text=None, blank=True, verbose_name='Materials Ja')
    materials = TranslatedField("materials")

    # We can't use the focal position here as this is for object-fit and object-position on the
    # browser side and this doesn't zoom into the box like wagtail's cropping. So simply have
    # options for choosing the preferred edge.
    horizontal_position = models.CharField(
        choices=[
            ("left", "Left"),
            ("center", "Center"),
            ("right", "Right"),
        ],
        default="center",
        help_text="When cropped in an image grid which horizontal side is preferred",
        max_length=10,
    )
    vertical_position = models.CharField(
        choices=[
            ("bottom", "Bottom"),
            ("center", "Center"),
            ("top", "Top"),
        ],
        default="center",
        help_text="When cropped in an image grid which vertical side is preferred",
        max_length=10,
    )

    @cached_property
    def get_materials(self):
        return self.materials.values_list("name", flat=True).order_by("name")

    admin_form_fields = (
        'title',
        'title_ja',
        'concept_en',
        'concept_ja',
        'materials_en',
        'materials_ja',
        'horizontal_position',
        'vertical_position',
        'file',
        'collection',
        'focal_point_x',
        'focal_point_y',
        'focal_point_width',
        'focal_point_height',
    )

    @cached_property
    def title_en(self):
        return self.title


class YumekaRendition(AbstractRendition):
    image = models.ForeignKey(YumekaImage, related_name='renditions',
                              on_delete=models.CASCADE)

    class Meta:
        unique_together = (
            ('image', 'filter_spec', 'focal_point_key'),
        )


# Delete the source image file when an image is deleted
@receiver(pre_delete, sender=YumekaImage)
def image_delete(sender, instance, **kwargs):
    instance.file.delete(False)


# Delete the rendition image file when a rendition is deleted
@receiver(pre_delete, sender=YumekaRendition)
def rendition_delete(sender, instance, **kwargs):
    instance.file.delete(False)
