#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2017, 2018, 2020
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.functional import cached_property
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
# from wagtail.snippets.models import register_snippet


def validate_colour(value):
    if not value.startswith("#"):
        raise ValidationError("Colour should start with a '#'")

    # Check if length is #RGB or #RRGGBB
    if len(value) != 4 and len(value) != 7:
        raise ValidationError("Length is incorrect, should be either #FFF or #FFFFFF")

    # TODO: check hex values are correct


# @register_snippet  # disable styling snippet for now
class StyleSnippet(models.Model):
    title = models.CharField(max_length=255, help_text="A title of 'default', is the default across the site")
    background_colour = models.CharField(
        default="#40A471",
        max_length=7,
        help_text="Eg '#FFFFFF' for white",
        validators=[
            validate_colour,
        ],
    )
    text_colour = models.CharField(
        default="#000000",
        max_length=7,
        help_text="Eg '#FFFFFF' for white",
        validators=[
            validate_colour,
        ],
    )
    shade_colour = models.CharField(
        default="#FFFFFF",
        max_length=7,
        help_text="Eg '#FFFFFF' for white",
        validators=[
            validate_colour,
        ],
    )
    shade_transparency = models.IntegerField(
        default=75,
        validators=[
            MaxValueValidator(100),
            MinValueValidator(0),
        ],
        help_text="Transparency of the shade, between 0 and 100.",
    )
    background_image = models.ForeignKey(
        'image.YumekaImage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    overlay_image = models.ForeignKey(
        'image.YumekaImage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name="Overlay image (width 300 pixels)",
    )

    shade_image = models.ForeignKey(
        'image.YumekaImage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name="Side shade image (width 300 pixels)",
    )

    shade_mobile_image = models.ForeignKey(
        'image.YumekaImage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name="Side shade mobile image (height 300 pixels)",
    )

    panels = [
        FieldPanel("title"),
        FieldPanel("background_colour"),
        ImageChooserPanel("background_image"),
        ImageChooserPanel("overlay_image"),
        ImageChooserPanel("shade_image"),
        ImageChooserPanel("shade_mobile_image"),
        FieldPanel("text_colour"),
        FieldPanel("shade_colour"),
        FieldPanel("shade_transparency"),
    ]

    def __str__(self):
        return self.title

    @cached_property
    def shade_hex_colour(self):
        rgb = self.shade_colour[1:]

        if len(rgb) == 3:
            rgb = [int(code, 16) * 16 for code in rgb]
        else:
            rgb = [int(rgb[i:i + 2], 16) for i in range(0, 6, 2)]

        return "rgba(" + ",".join(map(str, rgb)) + "," + str(self.shade_transparency / 100) + ")"
