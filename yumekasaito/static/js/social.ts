// Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/*! This Executable Form is subject to Mozilla Public License, v. 2.0.
 *! The Source Code Form is available at https://gitlab.com/ahayzen/yumekasaito.com
 *! Copyright 2020 Andrew Hayzen <ahayzen@gmail.com> */

const copyUrlToClipboard: Array<CopyURLToClipboard> = [];
const mobileShareBar: Array<MobileShareButton> = [];

class CopyURLToClipboard {
    private anchor: HTMLAnchorElement;
    // Use instanced event listeners so that "this" is always correct
    private clickListener: EventListener;
    private mouseLeaveListener: EventListener;

    constructor(anchor: HTMLAnchorElement) {
        this.anchor = anchor;
        this.clickListener = (event: Event) => this.onClick(event);
        this.mouseLeaveListener = () => this.onMouseLeave();

        anchor.addEventListener("click", this.clickListener);
        anchor.addEventListener("mouseleave", this.mouseLeaveListener);
    }

    private onClick(event: Event) {
        if (navigator.clipboard) {
            navigator.clipboard.writeText(document.URL).then(this.onSuccess.bind(this), () => {
                console.warn("Could not copy URL to clipboard.");
            });

            event.preventDefault();
        } else {
            console.warn("navigator.clipboard is not available, site needs to be https or localhost");
        }
    }

    private onMouseLeave() {
        this.anchor.dataset["label"] = this.anchor.dataset["labelCopy"];
    }

    private onSuccess() {
        this.anchor.dataset["label"] = this.anchor.dataset["labelCopied"];
    }
}

class MobileShareButton {
    static mobileHiddenColor: string = "transparent";
    static mobileShownClassName: string = "mobile-shown";
    static mobileShownColor: string = "rgb(255, 255, 255)";

    // Use instanced event listeners so that "this" is always correct
    private anchorClickEventListener: EventListener;
    private documentClickEventListener: EventListener;
    private ul: HTMLUListElement;

    public constructor(anchor: HTMLAnchorElement) {
        this.anchorClickEventListener = (event: Event) => this.onAnchorClick(event);
        this.documentClickEventListener = (event: Event) => this.onDocumentClick(event);
        this.ul = (anchor.parentElement as HTMLLIElement).parentElement as HTMLUListElement;

        anchor.addEventListener("click", this.anchorClickEventListener);
    }

    private hide() {
        this.ul.classList.remove(MobileShareButton.mobileShownClassName);
        this.ul.style.backgroundColor = MobileShareButton.mobileHiddenColor;
    }

    private onAnchorClick(event: Event) {
        this.show();

        document.addEventListener("click", this.documentClickEventListener);
        document.addEventListener("scroll", this.documentClickEventListener);

        event.preventDefault();
    }

    private onDocumentClick(event: Event) {
        if (!this.ul.contains(event.target as Node)) {
            this.hide();

            document.removeEventListener("click", this.documentClickEventListener);
            document.removeEventListener("scroll", this.documentClickEventListener);

            event.preventDefault();
        }
    }

    private show() {
        this.ul.classList.add(MobileShareButton.mobileShownClassName);
        this.ul.style.backgroundColor = MobileShareButton.mobileShownColor;
    }
}

window.addEventListener("DOMContentLoaded", function () {
    // Find all the bindable copy url anchors
    for (let anchor of document.querySelectorAll<HTMLAnchorElement>("a[data-bind='copy-url']")) {
        copyUrlToClipboard.push(new CopyURLToClipboard(anchor));
    }

    // Find all the bindable mobile share buttons
    for (let anchor of document.querySelectorAll<HTMLAnchorElement>("a[data-bind='mobile-share']")) {
        mobileShareBar.push(new MobileShareButton(anchor));
    }
});
