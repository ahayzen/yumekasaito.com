// Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/*! This Executable Form is subject to Mozilla Public License, v. 2.0.
 *! The Source Code Form is available at https://gitlab.com/ahayzen/yumekasaito.com
 *! Copyright 2020 Andrew Hayzen <ahayzen@gmail.com> */

// This works by performing the following actions
// - Show modal dialog with a given image
//   - user clicks on an image collection item
//   - user selects back (popstate) and #modal+n is in the href hash
//   - user opens a URL with #modal+n in the href hash
// - Hide modal dialog
//   - user clicks on the modal dialog to request hiding
//   - user selects back (popstate) and href hash doesn't contain #modal+n
//   - user opens a URL without #modal+n in the href hash or an invalid n

function autoShowModal(event: Event) {
    // If the location has a modal then show it, otherwise ensure the modal is hidden
    if (document.location.hash.startsWith("#modal+")) {
        const imageGridId: number = parseInt(document.location.hash.substr(document.location.hash.indexOf("+") + 1));

        if (showModal(imageGridId, event)) {
            return;
        }
    }

    hideImageGridModalItem();
    showArticles();
}

function hrefWithHash(hash: string | null): string {
    if (hash) {
        return document.location.href.substr(0, document.location.href.indexOf("#")) + "#" + hash;
    } else {
        return document.location.href.substr(0, document.location.href.indexOf("#"));
    }
}

function hideImageGridModalItem(): void {
    // Remove the modal code if it exists
    let elm: HTMLElement | null = document.getElementById("modal");
    elm?.parentNode?.removeChild(elm);
}

function hideImageGridModal(imageGridId: number): void {
    // Hide the modal
    hideImageGridModalItem();
    showArticles();

    // Scroll to imageGridId position
    let anchor: HTMLAnchorElement | null = document.querySelector<HTMLAnchorElement>("a[data-image-grid-id='" + imageGridId.toString() + "']");
    anchor?.scrollIntoView({ block: "center" });

    // Remove the #modal+gridId from the URL if matches the modal we are hiding
    if (document.location.hash == "#modal+" + imageGridId.toString()) {
        history.pushState(null, "", hrefWithHash(null));
    }
}

function imageGridModalClicked(figure: HTMLElement, event: MouseEvent, imageGridId: number): void {
    const imageCount: number = document.querySelectorAll<HTMLAnchorElement>("a[data-bind='image-grid-modal']").length;
    const rect: DOMRect = figure.getBoundingClientRect();
    const xPercent: number = (event.clientX - rect.x) / rect.width;
    const yPercent: number = (event.clientY - rect.y) / rect.height;

    if (imageCount > 1 && xPercent < 0.2) {
        showModal(imageGridId > 0 ? imageGridId - 1 : imageCount - 1, event);
    } else if (imageCount > 1 && xPercent > 0.8 && yPercent > 0.2) {
        showModal((imageGridId + 1) % imageCount, event);
    } else if (yPercent < 0.2) {
        hideImageGridModal(imageGridId);
    } else if (imageCount > 1) {
        toggleControlsMobile();
    } else {
        hideImageGridModal(imageGridId);
    }
}

function showArticles(): void {
    for (let article of document.getElementsByTagName("article")) {
        article.style.display = "";
    }
}

function showImageGridModal(this: HTMLAnchorElement, event: Event) {
    const multipleImages: boolean = document.querySelectorAll<HTMLAnchorElement>("a[data-bind='image-grid-modal']").length > 1;

    // Prevent the href being followed from the click event
    event.preventDefault();

    // Ensure any modals are hidden
    hideImageGridModalItem();

    // Insert the modal
    document.body.insertAdjacentHTML("beforeend", "\
<figure id=\"modal\" onclick=\"imageGridModalClicked(this, event, " + this.dataset.imageGridId + ");\">\
    <picture" + (multipleImages ? " class=\"multiple\"" : "") + ">\
        <img alt=\"" + this.dataset.imageName + "\" height=\"" + this.dataset.imageHeight + "\" src=\"" + this.dataset.imageSrc + "\" width=\"" + this.dataset.imageWidth + "\" />\
    </picture>\
    <figcaption>\
        <h2>" + this.dataset.imageName + "</h2>\
        " + (this.dataset.imageMaterials ? "<h3>" + this.dataset.imageMaterials + "</h3>" : "") + "\
        " + (this.dataset.imageConcept ? "<p>" + this.dataset.imageConcept + "</p>" : "") + "\
    </figcaption>\
</figure>");

    // Hide any articles
    for (let article of document.getElementsByTagName("article")) {
        article.style.display = "none";
    }

    // Scroll the modal into view
    document.getElementById("modal")?.scrollIntoView({ block: "start" });

    // Store the modal information in the url
    // this is so that pressing back will work
    //
    // Do not store this if it's the current hash, as this means we are loading
    // this page and don't want to add to the stack
    if (document.location.hash != "#modal+" + this.dataset.imageGridId) {
        history.pushState(null, "", hrefWithHash("modal+" + this.dataset.imageGridId));
    }

    // Ensure the event does not propagate
    return false;
}

function showModal(imageGridId: number, event: Event): boolean {
    const anchor: HTMLAnchorElement | null = document.querySelector<HTMLAnchorElement>("a[data-image-grid-id='" + imageGridId + "']");
    if (anchor) {
        showImageGridModal.bind(anchor)(event);
        return true;
    } else {
        return false;
    }
}

function toggleControlsMobile() {
    const figure: HTMLElement | null = document.getElementById("modal");

    if (figure) {
        const pictures: HTMLCollectionOf<HTMLPictureElement> = figure.getElementsByTagName("picture");

        if (pictures.length > 0) {
            pictures[0].classList.toggle("controls");
        }
    }
}

window.addEventListener("DOMContentLoaded", function (event: Event) {
    // Counter for id's of each image to bind to
    let i: number = 0;

    // Find all the bindable images for image grid modal
    for (let anchor of document.querySelectorAll<HTMLAnchorElement>("a[data-bind='image-grid-modal']")) {
        // Set an id to each anchor
        anchor.dataset["imageGridId"] = i.toString();

        // Connect the click event to show the image grid modal
        anchor.addEventListener("click", showImageGridModal.bind(anchor));

        // Increment id counter
        i += 1;
    }

    // Check if any modals need to be shown from the URL
    autoShowModal(event);
});

window.addEventListener("keydown", function (event: KeyboardEvent) {
    // If a modal is shown then provide keyboard shortcuts
    if (document.location.hash.startsWith("#modal+")) {
        const imageGridId: number = parseInt(document.location.hash.substr(document.location.hash.indexOf("+") + 1));
        const imageCount: number = document.querySelectorAll<HTMLAnchorElement>("a[data-bind='image-grid-modal']").length;

        if (event.key == "ArrowLeft") {
            showModal(imageGridId > 0 ? imageGridId - 1 : imageCount - 1, event);
        } else if (event.key == "ArrowRight") {
            showModal((imageGridId + 1) % imageCount, event);
        } else if (event.key == "Escape") {
            hideImageGridModal(imageGridId);
        }
    }
});

// When back is pressed, see if any modals needs to be shown or hidden
window.addEventListener("popstate", function (event: PopStateEvent) {
    event.preventDefault();
    autoShowModal(event);

    return false;
});
