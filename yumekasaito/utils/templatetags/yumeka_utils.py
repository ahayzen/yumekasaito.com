#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2016, 2018, 2020
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django import template
from django.conf import settings
from django.urls import translate_url as django_translate_url
from wagtail.core.models import Site
from yumekasaito.blog.models import BlogPage

register = template.Library()


@register.filter
def get_class(obj):
    return obj.__class__.__name__


@register.simple_tag
def get_google_analytics_code():
    return getattr(settings, "GOOGLE_ANALYTICS_CODE", "")


@register.simple_tag
def get_google_site_verification_code():
    return getattr(settings, "GOOGLE_SITE_VERIFICATION", "")


@register.simple_tag(takes_context=True)
def get_menu_items(context):
    return get_root_page(context).get_children().live().in_menu().specific()


@register.simple_tag(takes_context=True)
def get_menu_item_children(context, page):
    return page.get_children().not_exact_type(BlogPage).live().in_menu().specific()


@register.simple_tag(takes_context=True)
def get_root_page(context):
    return Site.find_for_request(context['request']).root_page


@register.simple_tag(takes_context=True)
def is_child_or_self(context, child_page, parent_page):
    # child_page can be of type str when visiting root page
    # so check we have is_descendant_of attribute otherwise we cause 500 errors
    return hasattr(child_page, "is_descendant_of") and child_page.is_descendant_of(parent_page) or child_page == parent_page


@register.simple_tag(takes_context=True)
def translate_url(context, lang_code):
    url = context["request"].path
    return django_translate_url(url, lang_code)
