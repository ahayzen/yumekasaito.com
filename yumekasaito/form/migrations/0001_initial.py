# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-20 22:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.documents.blocks
import wagtail.embeds.blocks
import wagtail.images.blocks


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('wagtailcore', '0032_add_bulk_delete_page_permission'),
    ]

    operations = [
        migrations.CreateModel(
            name='FormField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('field_type', models.CharField(choices=[('singleline', 'Single line text'), ('multiline', 'Multi-line text'), ('email', 'Email'), ('number', 'Number'), ('url', 'URL'), ('checkbox', 'Checkbox'), ('checkboxes', 'Checkboxes'), ('dropdown', 'Drop down'), ('radio', 'Radio buttons'), ('date', 'Date'), ('datetime', 'Date/time')], max_length=16, verbose_name='field type')),
                ('required', models.BooleanField(default=True, verbose_name='required')),
                ('default_value', models.CharField(blank=True, help_text='Default value. Comma separated values supported for checkboxes.', max_length=255, verbose_name='default value')),
                ('help_text', models.CharField(blank=True, max_length=255, verbose_name='help text')),
                ('label_en', models.CharField(max_length=255)),
                ('label_ja', models.CharField(max_length=255)),
                ('choices_en', models.TextField(blank=True, help_text='Comma separated list of choices. Only applicable in checkboxes, radio and dropdown.')),
                ('choices_ja', models.TextField(blank=True, help_text='Comma separated list of choices. Only applicable in checkboxes, radio and dropdown.')),
            ],
            options={
                'abstract': False,
                'ordering': ['sort_order'],
            },
        ),
        migrations.CreateModel(
            name='FormPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('body', wagtail.core.fields.StreamField((('heading', wagtail.core.blocks.StructBlock((('field_en', wagtail.core.blocks.CharBlock(label='English')), ('field_ja', wagtail.core.blocks.CharBlock(label='Japanese'))), form_classname='full title', icon='title')), ('paragraph', wagtail.core.blocks.StructBlock((('field_en', wagtail.core.blocks.RichTextBlock(label='English')), ('field_ja', wagtail.core.blocks.RichTextBlock(label='Japanese'))), icon='pilcrow')), ('bullet_list', wagtail.core.blocks.StructBlock((('items', wagtail.core.blocks.ListBlock(wagtail.core.blocks.StructBlock((('field_en', wagtail.core.blocks.CharBlock(label='English')), ('field_ja', wagtail.core.blocks.CharBlock(label='Japanese'))), label='Item'))),), icon='list-ul')), ('numbered_list', wagtail.core.blocks.StructBlock((('items', wagtail.core.blocks.ListBlock(wagtail.core.blocks.StructBlock((('field_en', wagtail.core.blocks.CharBlock(label='English')), ('field_ja', wagtail.core.blocks.CharBlock(label='Japanese'))), label='Item'))),), icon='list-ol')), ('hr', wagtail.core.blocks.StructBlock((('full_width', wagtail.core.blocks.BooleanBlock(help_text='Should the horizontal rule span the full width of thepage, or content', required=False)),), icon='horizontalrule')), ('link', wagtail.core.blocks.PageChooserBlock(icon='link')), ('image', wagtail.images.blocks.ImageChooserBlock(icon='image')), ('embed', wagtail.embeds.blocks.EmbedBlock(icon='media')), ('document', wagtail.documents.blocks.DocumentChooserBlock(icon='doc-empty')), ('image_collection', wagtail.core.blocks.StructBlock((('images', wagtail.core.blocks.ListBlock(wagtail.images.blocks.ImageChooserBlock(), label='Images')),), icon='image')), ('raw_html', wagtail.core.blocks.RawHTMLBlock(icon='code'))))),
                ('title_ja', models.CharField(max_length=255, verbose_name='Title Japanese')),
                ('to_address', models.CharField(blank=True, help_text='Optional - form submissions will be emailed to these addresses. Separate multiple addresses by comma.', max_length=255, verbose_name='to address')),
                ('from_address', models.CharField(blank=True, max_length=255, verbose_name='from address')),
                ('subject', models.CharField(blank=True, max_length=255, verbose_name='subject')),
                ('landing_page_text', wagtail.core.fields.StreamField((('heading', wagtail.core.blocks.StructBlock((('field_en', wagtail.core.blocks.CharBlock(label='English')), ('field_ja', wagtail.core.blocks.CharBlock(label='Japanese'))), form_classname='full title', icon='title')), ('paragraph', wagtail.core.blocks.StructBlock((('field_en', wagtail.core.blocks.RichTextBlock(label='English')), ('field_ja', wagtail.core.blocks.RichTextBlock(label='Japanese'))), icon='pilcrow')), ('bullet_list', wagtail.core.blocks.StructBlock((('items', wagtail.core.blocks.ListBlock(wagtail.core.blocks.StructBlock((('field_en', wagtail.core.blocks.CharBlock(label='English')), ('field_ja', wagtail.core.blocks.CharBlock(label='Japanese'))), label='Item'))),), icon='list-ul')), ('numbered_list', wagtail.core.blocks.StructBlock((('items', wagtail.core.blocks.ListBlock(wagtail.core.blocks.StructBlock((('field_en', wagtail.core.blocks.CharBlock(label='English')), ('field_ja', wagtail.core.blocks.CharBlock(label='Japanese'))), label='Item'))),), icon='list-ol')), ('hr', wagtail.core.blocks.StructBlock((('full_width', wagtail.core.blocks.BooleanBlock(help_text='Should the horizontal rule span the full width of thepage, or content', required=False)),), icon='horizontalrule')), ('link', wagtail.core.blocks.PageChooserBlock(icon='link')), ('image', wagtail.images.blocks.ImageChooserBlock(icon='image')), ('embed', wagtail.embeds.blocks.EmbedBlock(icon='media')), ('document', wagtail.documents.blocks.DocumentChooserBlock(icon='doc-empty')), ('image_collection', wagtail.core.blocks.StructBlock((('images', wagtail.core.blocks.ListBlock(wagtail.images.blocks.ImageChooserBlock(), label='Images')),), icon='image')), ('raw_html', wagtail.core.blocks.RawHTMLBlock(icon='code'))), blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page', models.Model),
        ),
        migrations.AddField(
            model_name='formfield',
            name='page',
            field=modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='form_fields', to='form.FormPage'),
        ),
    ]
