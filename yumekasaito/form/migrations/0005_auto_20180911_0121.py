# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2018-09-11 00:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('form', '0004_auto_20180212_0115'),
    ]

    operations = [
        migrations.AddField(
            model_name='formpage',
            name='menu_title_en',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Menu Title English - Shorter version of title'),
        ),
        migrations.AddField(
            model_name='formpage',
            name='menu_title_ja',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Menu Title Japanese - Shorter version of title'),
        ),
    ]
