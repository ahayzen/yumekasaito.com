# Generated by Django 2.2.11 on 2020-05-15 22:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('form', '0013_auto_20200515_2126'),
    ]

    operations = [
        migrations.AddField(
            model_name='formpage',
            name='automatic_title_heading',
            field=models.BooleanField(default=True, help_text='Should a header in the body be created from the title?'),
        ),
    ]
