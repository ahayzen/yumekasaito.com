#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2017, 2018, 2020
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from captcha.fields import ReCaptchaField
from django.db import models
from django.utils import translation
from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import (
    FieldPanel, InlinePanel, MultiFieldPanel, StreamFieldPanel
)
from wagtail.core.fields import StreamField
from wagtail.contrib.forms.models import AbstractFormField

from wagtailcaptcha.forms import WagtailCaptchaFormBuilder
from wagtailcaptcha.models import WagtailCaptchaEmailForm

from yumekasaito.base.blocks import YumekaStreamBlock
from yumekasaito.base.models import (
    YumekaBodyMixin, YumekaListingImageMixin, YumekaTitleTranslationMixin,
    YumekaSitemapTranslationMixin,
)
from yumekasaito.base.translation import TranslatedField


class FormField(AbstractFormField):
    page = ParentalKey('FormPage', related_name='form_fields')

    # Override label and choices to make them translatable
    label_en = models.CharField(max_length=255)
    label_ja = models.CharField(max_length=255)
    label = TranslatedField("label")

    choices_en = models.TextField(
        blank=True,
        help_text='Comma separated list of choices. Only applicable in checkboxes, radio and dropdown.'
    )
    choices_ja = models.TextField(
        blank=True,
        help_text='Comma separated list of choices. Only applicable in checkboxes, radio and dropdown.'
    )
    choices = TranslatedField("choices")

    panels = [
        FieldPanel('label_en'),
        FieldPanel('label_ja'),
        FieldPanel('required'),
        FieldPanel('field_type', classname="formbuilder-type"),
        FieldPanel('choices_en', classname="formbuilder-choices"),
        FieldPanel('choices_ja', classname="formbuilder-choices"),
    ]


# Translation aware wagtail captcha form builder
#
# This overrides the formfields method in WagtailCaptchaFormBuilder
# https://github.com/springload/wagtail-django-recaptcha/blob/da39697d16499658f2ef954bfb98baae34df119b/wagtailcaptcha/forms.py#L12
# which allows us to inject the language into the ReCaptchaField's widget,
# which is ReCaptchaBase that has api_params
# https://github.com/praekelt/django-recaptcha/blob/97467c22f2d64025184b79f9714ad2f5002164f0/captcha/widgets.py#L36
# these then match the arguments that can be given to Google recaptcha
# https://developers.google.com/recaptcha/docs/display#javascript_resource_apijs_parameters
class TranslationWagtailCaptchaFormBuilder(WagtailCaptchaFormBuilder):
    @property
    def formfields(self):
        # Add wagtailcaptcha to formfields property
        fields = super(WagtailCaptchaFormBuilder, self).formfields

        field = ReCaptchaField(label='')
        field.widget.api_params["hl"] = translation.get_language()
        fields[self.CAPTCHA_FIELD_NAME] = field

        return fields


class FormPage(YumekaSitemapTranslationMixin, WagtailCaptchaEmailForm,
               YumekaListingImageMixin, YumekaTitleTranslationMixin,
               YumekaBodyMixin):
    form_builder = TranslationWagtailCaptchaFormBuilder

    def __init__(self, *args, **kwargs):
        super(FormPage, self).__init__(*args, **kwargs)

    show_in_menus_default = True

    landing_page_text = StreamField(YumekaStreamBlock(), blank=True)

    content_panels = YumekaTitleTranslationMixin.content_panels \
        + YumekaBodyMixin.content_panels + [
            StreamFieldPanel("landing_page_text"),
            InlinePanel('form_fields', label="Form fields"),  # fields in form
            MultiFieldPanel([
                FieldPanel('to_address'),
                FieldPanel('from_address'),
                FieldPanel('subject'),
            ], "Email")
        ]

    promote_panels = [
        FieldPanel("slug"),
        FieldPanel("show_in_menus"),
    ] + YumekaListingImageMixin.promote_panels

    settings_panels = WagtailCaptchaEmailForm.settings_panels

    class Meta:
        verbose_name = "Form"
