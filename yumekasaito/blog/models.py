#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2020
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.db import models
from django.shortcuts import render
from django.utils import translation
from django.utils.functional import cached_property

from wagtail.images.edit_handlers import ImageChooserPanel

from yumekasaito.base.models import (
    YumekaPage, YumekaBodyMixin, YumekaListingImageMixin,
    YumekaPaginationMixin,
)


class BlogIndexPage(YumekaPage, YumekaListingImageMixin,
                    YumekaPaginationMixin):
    parent_page_types = [
        "home.HomePage",
    ]
    subpage_types = [
        "blog.BlogPage",
    ]

    # Override YumekaTitleTranslationMixin so we default to False
    automatic_title_heading = models.BooleanField(
        default=False,
        help_text="Should a header in the body be created from the title?")

    content_panels = (
        YumekaPage.content_panels
    )

    promote_panels = (
        YumekaPage.promote_panels + YumekaListingImageMixin.promote_panels
    )

    search_fields = (
        YumekaPage.search_fields
    )

    @cached_property
    def children(self):
        return self.get_children().live().specific().order_by("-first_published_at")

    def serve(self, request, *args, **kwargs):
        paginated_children = self.get_paginator_results(request, self.children)

        return render(
            request,
            self.template,
            {
                'self': self,
                'children': paginated_children,
            }
        )

    class Meta:
        verbose_name = "Blog Index"


class BlogPage(YumekaPage, YumekaListingImageMixin, YumekaBodyMixin):
    parent_page_types = [
        "blog.BlogIndexPage",
    ]
    subpage_types = [
    ]

    content_panels = (
        YumekaPage.content_panels +
        YumekaBodyMixin.content_panels
    )

    promote_panels = (
        YumekaPage.promote_panels + YumekaListingImageMixin.promote_panels
    )

    search_fields = (
        YumekaPage.search_fields +
        YumekaBodyMixin.search_fields
    )

    @cached_property
    def body_as_text(self):
        language = translation.get_language()
        text = ""

        for block in self.body.stream_data:
            if block["type"] == "paragraph":
                text += block["value"]["field_%s" % language]

        return text

    def get_next_live_sibling(self):
        return self.get_next_siblings(
            inclusive=False,
        ).filter(live=True).first()

    def get_previous_live_sibling(self):
        return self.get_prev_siblings(
            inclusive=False,
        ).filter(live=True).first()

    class Meta:
        verbose_name = "New Blog Entry"
