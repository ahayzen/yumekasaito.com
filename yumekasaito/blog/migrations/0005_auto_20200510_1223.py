# Generated by Django 2.2.11 on 2020-05-10 11:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20200510_1222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogindexpage',
            name='listing_image',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='image.YumekaImage'),
        ),
        migrations.AlterField(
            model_name='blogpage',
            name='listing_image',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='image.YumekaImage'),
        ),
    ]
