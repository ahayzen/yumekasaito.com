#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.utils import translation
from wagtail.admin.edit_handlers import MultiFieldPanel, FieldPanel
from wagtail.core.blocks.struct_block import StructBlock


class TranslatedBlock(StructBlock):
    is_abstract = True

    def get_context(self, value, parent_context=None):
        context = super(TranslatedBlock, self).get_context(value, parent_context=parent_context)
        context["value"]["field"] = self.translated_field(value)

        return context

    def translated_field(self, value):
        language = translation.get_language()

        if language != "":
            return value.get("_".join(["field", language]), value["field_en"])
        else:
            return value["field_en"]

    class Meta:
        abstract = True


class TranslatedField(object):
    def __init__(self, field):
        self.field = field

    def __get__(self, instance, owner):
        language = translation.get_language()

        if language != "":
            return getattr(instance, "_".join([self.field, language]))
        else:
            return getattr(instance, "_".join([self.field, "en"]))


def translated_multi_field_panel(field, panel=FieldPanel):
    return MultiFieldPanel(
        [
            panel("_".join([field, lang]))
            for lang in ["en", "ja"]
        ],
        heading=field.title(),
    )
