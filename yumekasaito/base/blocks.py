#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2016, 2017, 2018, 2020
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""

from wagtail.core.blocks import (
    CharBlock, PageChooserBlock, RawHTMLBlock, RichTextBlock,
    StreamBlock
)
from wagtail.core.blocks.field_block import (
    BooleanBlock, ChoiceBlock, IntegerBlock,
)
from wagtail.core.blocks.list_block import ListBlock
from wagtail.core.blocks.struct_block import StructBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.embeds.blocks import EmbedBlock
from wagtail.images.blocks import ImageChooserBlock

from yumekasaito.base.translation import TranslatedBlock


class TranslatedCharBlock(TranslatedBlock):
    field_en = CharBlock(label="English")
    field_ja = CharBlock(label="Japanese")

    class Meta:
        icon = "title"
        template = "base/translated_char.html"


class TranslatedHeadingBlock(TranslatedBlock):
    field_en = CharBlock(label="English")
    field_ja = CharBlock(label="Japanese")

    class Meta:
        icon = "title"
        template = "base/translated_heading.html"


class TranslatedRichTextBlock(TranslatedBlock):
    field_en = RichTextBlock(label="English")
    field_ja = RichTextBlock(label="Japanese")

    class Meta:
        icon = "pilcrow"
        template = "base/translated_richtext.html"


class BulletListBlock(StructBlock):
    items = ListBlock(TranslatedCharBlock(label="Item"))

    class Meta:
        icon = "list-ul"
        template = "base/list_ul.html"


class NumberListBlock(StructBlock):
    items = ListBlock(TranslatedCharBlock(label="Item"))

    class Meta:
        icon = "list-ol"
        template = "base/list_ol.html"


class HrBlock(StructBlock):
    full_width = BooleanBlock(
        help_text=(
                "Should the horizontal rule span the full width of the"
                "page, or content"
            ),
        required=False
    )

    class Meta:
        icon = "horizontalrule"
        template = "base/hr.html"


class FloatingImageBlock(StructBlock):
    image = ImageChooserBlock()
    layout = ChoiceBlock(
        choices=[
            ("left", "Left"),
            ("right", "Right"),
        ],
        default="left",
        help_text="Which side should the image float on."
    )
    width = IntegerBlock(
        max_value=100,
        min_value=0,
        help_text="Width as a percentage of the page",
        default=50,
    )

    class Meta:
        icon = "image"
        template = "base/floating_image.html"


class ImageCollectionItem(StructBlock):
    image = ImageChooserBlock(label="Image", required=True)
    full_width = BooleanBlock(default=False, required=False)


class ImageCollectionBlock(StructBlock):
    images = ListBlock(ImageCollectionItem(), label="Images")

    class Meta:
        icon = "image"
        template = "base/image_collection.html"


class YumekaStreamBlock(StreamBlock):
    heading = TranslatedHeadingBlock(form_classname="full title", icon="title")
    paragraph = TranslatedRichTextBlock(icon="pilcrow")

    bullet_list = BulletListBlock(icon="list-ul")
    numbered_list = NumberListBlock(icon="list-ol")

    hr = HrBlock(icon="horizontalrule")

    link = PageChooserBlock(icon="link")

    image = ImageChooserBlock(icon="image", template="base/image.html")
    embed = EmbedBlock(icon="media")
    document = DocumentChooserBlock(icon="doc-empty")

    image_collection = ImageCollectionBlock(icon="image")

    floating_image = FloatingImageBlock(icon="image")

    raw_html = RawHTMLBlock(icon="code")
