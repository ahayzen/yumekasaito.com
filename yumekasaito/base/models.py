#!/usr/bin/python3
"""
This file is part of yumekasaito.com

Copyright (C) 2016, 2017, 2018, 2020
    Andrew Hayzen <ahayzen@gmail.com>

yumekasaito.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

yumekasaito.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with yumekasaito.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.conf import settings
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import models
from django.utils import translation
from django.utils.functional import cached_property
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, \
    StreamFieldPanel
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search.index import SearchField

from yumekasaito.base.blocks import YumekaStreamBlock
from yumekasaito.base.translation import TranslatedField


class YumekaBodyMixin(models.Model):
    is_abstract = True

    # We need to set both blank and required
    # https://github.com/wagtail/wagtail/issues/4306
    body = StreamField(YumekaStreamBlock(required=False), blank=True)

    content_panels = [
        StreamFieldPanel("body"),
    ]

    search_fields = [
        SearchField("body", partial_match=True, boost=1),
    ]

    class Meta:
        abstract = True


class YumekaListingImageMixin(models.Model):
    is_abstract = True

    listing_image = models.ForeignKey(
        'image.YumekaImage',
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    promote_panels = [
        ImageChooserPanel("listing_image"),
    ]

    class Meta:
        abstract = True


class YumekaPaginationMixin(models.Model):
    is_abstract = True
    paginate_by = 15

    def get_paginator_results(self, request, items):
        paginator = Paginator(items, self.paginate_by)

        page = request.GET.get('page')

        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            results = paginator.page(1)
        except EmptyPage:
            results = paginator.page(paginator.num_pages)

        return results

    class Meta:
        abstract = True


class YumekaSitemapTranslationMixin(models.Model):
    is_abstract = True

    def get_sitemap_urls(self, request=None):
        # Inject ja sitemap url
        languages = getattr(settings, "LANGUAGES", [("en", "English")])
        pages = []

        for (code, lang) in languages:
            with translation.override(code):
                pages.append({
                    "lastmod": self.latest_revision_created_at,
                    "location": self.full_url,
                })

        return pages

    class Meta:
        abstract = True


class YumekaTitleTranslationMixin(models.Model):
    is_abstract = True

    title_ja = models.CharField(max_length=255, verbose_name="Title Japanese")

    translated_title = TranslatedField("title")

    automatic_title_heading = models.BooleanField(
        default=True,
        help_text="Should a header in the body be created from the title?")

    content_panels = [
        MultiFieldPanel(
            [
                FieldPanel("title"),
                FieldPanel("title_ja"),
            ],
            heading="Title"
        ),
        FieldPanel("automatic_title_heading"),
    ]

    search_fields = [
        # Boost title so it is above body
        # declare after as we are overriding
        SearchField("title", partial_match=True, boost=5),
        SearchField("title_ja", partial_match=True, boost=5),
    ]

    @cached_property
    def title_en(self):
        return self.title

    class Meta:
        abstract = True


class YumekaPage(YumekaSitemapTranslationMixin, Page,
                 YumekaTitleTranslationMixin):
    def __init__(self, *args, **kwargs):
        super(YumekaPage, self).__init__(*args, **kwargs)

    is_abstract = True
    show_in_menus_default = True

    content_panels = YumekaTitleTranslationMixin.content_panels + [
    ]

    promote_panels = [
        FieldPanel('slug'),
        FieldPanel('show_in_menus'),
    ]

    search_fields = YumekaTitleTranslationMixin.search_fields + [
    ]

    settings_panels = Page.settings_panels

    @cached_property
    def full_url(self):
        return self.get_site().root_url + self.url

    class Meta:
        abstract = True
