Django>=2.2,<2.3
wagtail>=2.11,<2.12

# Recaptcha
Django-recaptcha>=2.0,<2.1
wagtail-django-recaptcha>=1.0,<1.1

# SASS requirements
libsass>=0.19,<0.20
django-libsass>=0.7,<0.8
